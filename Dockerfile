FROM chill_base_php

COPY ./bin    /var/www/app/bin/.
COPY ./config    /var/www/app/config/.
COPY ./src    /var/www/app/src/.
COPY ./vendor /var/www/app/vendor/.
COPY ./public    /var/www/app/public/.
COPY ./composer.* /var/www/app/

COPY ./migrations /var/www/app/migrations/.
COPY ./templates /var/www/app/templates/.
COPY ./translations /var/www/app/translations/.

# import the manifest.json file
COPY ./public/build/manifest.json /var/www/app/public/build/manifest.json

ADD ./entrypoint.sh /.

RUN chmod 777 /entrypoint.sh

ENV PHP_FPM_USER=www-data \
    PHP_FPM_GROUP=www-data \
    GELF_HOST=gelf \
    GELF_PORT=12201 \
    APP_DEBUG=false \
    APP_ENV=prod \
    DATABASE_HOST=db \
    DATABASE_NAME=postgres \
    DATABASE_PORT=5432 \
    DATABASE_USER=postgres \
    RABBITMQ_HOST=rabbitmq \
    RABBITMQ_PORT=5672 \
    COMPOSER_HOME=/var/www/app/.composer

