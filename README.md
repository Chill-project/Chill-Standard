Chill - Standard Project
========================

This is the basic project you must install first in order to install Chill software.

The installation process is documented here: http://docs.chill.social/en/latest/installation/index.html

