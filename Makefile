THIS_FILE := $(lastword $(MAKEFILE_LIST))
PWD:=$(shell echo ${PWD})
UID:=$(shell id -u)
GID:=$(shell id -g)
DOCKERNODE_CMD=docker run --rm --user ${UID}:${GID} -v ${PWD}:/app --workdir /app -e YARN_CACHE_FOLDER=/app/.yarncache node:13
DOCKER_COMPOSE_PHP_EXEC_CMD=docker-compose run --user $(UID) -e CLEAR_CACHE=false php
PHP_BASE_IMAGE=php:7.3-fpm-buster
PHP_BASE_IMAGE_CHILL=chill_base_php
NGINX_BASE_IMAGE=nginx

help:
	@echo "Please make use of 'make <target>' where <target> is one of: "
	@echo "  build-assets           to build assets for production"
	@echo "  build-and-push-images  to build image PHP and NGINX and push them on a registry"
	@echo "  build-base-image-php   to build the base image for chill / php"
	@echo "  init                   to initialize your development directory"
	@echo "  install-vendor         to install vendor using composer"
	@echo "  migrate                to migrate the database to last version"
	@echo "  pull-base-image        to pull base image used in image php and nginx images"
	@echo "  post-install           to run post-install scripts"
	@echo "  post-update            to run post-update scripts"
	@echo "  upgrade-vendors        to upgrade vendor"

build-assets:
	$(DOCKERNODE_CMD) yarn install
	$(DOCKERNODE_CMD) yarn run encore production

build-base-image-php:
	docker build -t $(PHP_BASE_IMAGE_CHILL) docker/php/

init:
	@$(MAKE) -f $(THIS_FILE) build-base-image-php
	docker run --rm -e INSTALL_ID=$(UID) -e INSTALL_GID=$(GID) -e COMPOSER_HOME=/var/www/app/.composer --volume $(PWD):/var/www/app --workdir /var/www/app --entrypoint /usr/bin/env $(PHP_BASE_IMAGE_CHILL) /bin/bash ./docker/install.dev.sh
	docker run --rm -e INSTALL_ID=$(UID) -e INSTALL_GID=$(GID) -e COMPOSER_HOME=/var/www/app/.composer --volume $(PWD):/var/www/app --workdir /var/www/app --entrypoint /usr/bin/env --user $(UID) $(PHP_BASE_IMAGE_CHILL) php -d memory_limit=2G composer.phar upgrade --no-scripts
	@$(MAKE) -f $(THIS_FILE) build-assets
	@$(MAKE) -f $(THIS_FILE) post-install

post-install:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php -d memory_limit=2G composer.phar run-script post-install-cmd

post-update:
	docker-compose build php
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php -d memory_limit=2G composer.phar run-script post-update-cmd

migrate:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php bin/console doctrine:migration:migrate -n

push-images:
	docker-compose push php nginx

pull-base-images:
	docker pull $(PHP_BASE_IMAGE)
	docker pull $(NGINX_BASE_IMAGE)

build-and-push-images:
	@$(MAKE) -f $(THIS_FILE) build-base-image-php
	@$(MAKE) -f $(THIS_FILE) build-assets
	docker-compose build php nginx 
	@$(MAKE) -f $(THIS_FILE) push-images

upgrade-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php -d memory_limit=2G composer.phar upgrade

install-vendors:
	$(DOCKER_COMPOSE_PHP_EXEC_CMD) php -d memory_limit=2G composer.phar install
