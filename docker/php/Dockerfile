FROM php:7.4-fpm-buster

ENV PHPREDIS_VERSION 5.3.3

# Set the lifetime of a PHP session
ARG SESSION_LIFETIME=10800 
# default UID for the PHP user
ARG UID=1000
ARG GID=1000

RUN apt update && apt -y --no-install-recommends install wget gnupg  \
   && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc |  apt-key add - \
   && echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
   && apt update && apt -y --no-install-recommends install \
      libicu-dev  \
      g++ \
      postgresql-server-dev-10 \
      libzip-dev libzip4 unzip \
      libfreetype6-dev \
      libonig-dev `# install oniguruma, required for mbstring` \
      libpng-dev \
      libjpeg62-turbo-dev \
      git \
   && docker-php-ext-configure gd --with-freetype --with-jpeg \
   && docker-php-ext-install -j$(nproc) gd \
   && docker-php-ext-install intl pdo_pgsql mbstring zip bcmath sockets exif \
   && mkdir /tmp/redis \
   && curl -L -o /tmp/redis/redis.tar.gz https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz \
   && cd /tmp/redis \
   && tar xfz /tmp/redis/redis.tar.gz \
   && rm -r /tmp/redis/redis.tar.gz \
   && mkdir -p /usr/src/php/ext/redis \
   && mv /tmp/redis/phpredis-$PHPREDIS_VERSION/* /usr/src/php/ext/redis/. \
   && docker-php-ext-install redis \
   && apt remove -y wget libicu-dev g++ gnupg libzip-dev \
   && apt autoremove -y \
   && apt purge -y


RUN { \
    echo ""; \
    echo "[Date]"; \
    echo "date.timezone = Europe/Brussels"; \
    echo ""; \
    } >> /usr/local/etc/php/conf.d/date.ini

# register session in redis and store password
RUN { \
    echo ""; \
    echo "session.save_handler = redis" ; \
    echo "session.save_path = \"tcp://redis:6379?db=10\"" ; \
    echo "session.gc_maxlifetime = ${SESSION_LIFETIME}" ; \
    } >> /usr/local/etc/php/conf.d/custom.ini

WORKDIR /var/www/app

# ajoute des utilisateurs ayant le uid 1000
RUN groupadd --gid ${GID} "group${GID}" \
   && useradd --uid ${UID} --gid ${GID} --create-home "user${UID}"

ENTRYPOINT ["/entrypoint.sh"]

CMD ["php-fpm"]

